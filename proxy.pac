var FindProxyForURL = function(init, profiles) {
    return function(url, host) {
        "use strict";
        var result = init, scheme = url.substr(0, url.indexOf(":"));
        do {
            result = profiles[result];
            if (typeof result === "function") result = result(url, host, scheme);
        } while (typeof result !== "string" || result.charCodeAt(0) === 43);
        return result;
    };
}("+auto switch", {
    "+auto switch": function(url, host, scheme) {
        "use strict";
        if (/(?:^|\.)tuoitre\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)youtube\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)googlevideo\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)tinhte\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)vnexpress\.net$/.test(host)) return "DIRECT";
        if (/(?:^|\.)slack\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)slack-edge\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)ytimg\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)fpt\.com\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)fsoft\.com\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)dantri\.com\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)vnreview\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)wordpress\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)zalo\.me$/.test(host)) return "DIRECT";
        if (/(?:^|\.)zing\.vn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)zaloapp\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)mediacdn\.vn$/.test(host)) return "DIRECT";
        return "+__ruleListOf_auto switch";
    },
    "+__ruleListOf_auto switch": "+proxy",
    "+proxy": function(url, host, scheme) {
        "use strict";
        if (/^127\.0\.0\.1$/.test(host) || /^::1$/.test(host) || /^localhost$/.test(host)) return "DIRECT";
        return "PROXY hcm-proxy:9090";
    }
});